var express = require('express')
var router = express.Router();
const admin = require('firebase-admin');
const serviceAccountFile = require('./aquanov-adae6-firebase-adminsdk-cigrr-3f50d8fbff.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccountFile),
    databaseURL: "https://aquanov-adae6.firebaseio.com",
})
const database = admin.firestore();
router.get('/getList/:id', async (req, res) =>{
    if(req.params.id === '0') {
        const listOfSideBar = [];
        const getAll = await database.collection('HeaderBody').get().then((snapshot) => {
            snapshot.forEach((doc) => {
                listOfSideBar.push({
                    id: doc.id,
                    Text: doc.data(),
                })
              });
              })
        res.send(listOfSideBar);
    } else if (req.params.id === '1') {
        const listOfBodyHeaders = [];
        const getAll = await database.collection('SideBar').get().then((snapshot) => {
            snapshot.forEach((doc) => {
                listOfBodyHeaders.push({
                    id: doc.id,
                    Text: doc.data(),
                })
              });
              })
        res.send(listOfBodyHeaders);
    } else {
        res.sendStatus(404)
    }
});
router.post('/createHeader', async (req, res) => {
    const createHeader = await database.collection('HeaderBody').add({
        Header: req.body.Header,
        Description: req.body.Description
    })
    res.send(createHeader);
})
router.post('/createSideBarHedears', async (req, res) => {
    const createHeader = await database.collection('SideBar').add({
        Header: req.body.Header,
        Description: req.body.Description
    })
    res.send(createHeader);
})
router.put('/updateHeader', async (req, res) => {
    const updateHeader = await database.collection('HeaderBody').doc(req.body.id).set({
        Header: req.body.Header,
        Description: req.body.Description
    })
    res.send(updateHeader);
})
router.put('/updateSideBarHeader', async (req, res) => {
    const updateHeader = await database.collection('SideBar').doc(req.body.id).set({
        Header: req.body.Header,
        Description: req.body.Description
    })
    res.send(updateHeader);
})
router.delete('/:id/:idType', async (req, res) => {
    if (req.params.idType === '0') {
        const removeDetails = await database.collection('HeaderBody').doc(req.params.id).delete();
        res.send(removeDetails);
    } else if (req.params.idType === '1') {
        const removeDetails = await database.collection('SideBar').doc(req.params.id).delete();
        res.send(removeDetails);
    } else {
        res.sendStatus(404);
    }
})
module.exports = router