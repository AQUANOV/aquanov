const fireBase = require('firebase');
async function initializeApp() {
    const fireBaseStart = await fireBase.initializeApp({
        apiKey: "AIzaSyDA71zHB6LCxXaRq6AFkodIxN3-PCh8t8Y",
        authDomain: "aquanov-adae6.firebaseapp.com",
        databaseURL: "https://aquanov-adae6.firebaseio.com",
        projectId: "aquanov-adae6",
        storageBucket: "aquanov-adae6.appspot.com",
        messagingSenderId: "935395872472"
    })
    return fireBaseStart;
}
exports.initializeApp = initializeApp;