const express = require('express');
const app = express();
const port = 3000;
const basicRoutes = require('./aquaRoutes');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
startApp();
app.use('', basicRoutes);
async function startApp() {
    try {
        app.listen(port, () => console.log(`Aqua listing on ${port}!`))
    } catch (error) {
        console.log('cc', error);
    }   
}

